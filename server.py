#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os
import yaml
import flwr as fl
import numpy as np

from pathlib import Path
from logging import INFO, DEBUG
from flwr.common.logger import log
from sklearn.linear_model import LinearRegression, LogisticRegression


# Define output directories
experiment_dir = Path( 'results' )
history_dir = experiment_dir / 'history'


def fit_round( server_round ):
    return { 'server_round': server_round }

def set_initial_params( model ):
    model.coef_ = np.zeros( ( 1, 4 ) )
    if model.fit_intercept:
        model.intercept_ = np.zeros( ( 1, ) )

class SaveModelStrategy( fl.server.strategy.FedAvg ):
    def aggregate_fit( self, server_round, results, failures ):
        aggregated_parameters, aggregated_metrics = super().aggregate_fit( server_round, results, failures )

        if aggregated_parameters is not None:
            # Convert `Parameters` to `List[np.ndarray]`
            aggregated_ndarrays: List[np.ndarray] = fl.common.parameters_to_ndarrays( aggregated_parameters )

            # Save aggregated_ndarrays
            print( f"Saving round {server_round} aggregated_ndarrays ( {experiment_dir}/round-{server_round}-weights.npz ..." )
            np.savez( experiment_dir / f"round-{server_round}-weights.npz", *aggregated_ndarrays )
            print( '... done' )

        return aggregated_parameters, aggregated_metrics


if __name__ == '__main__':
    log( DEBUG, 'experiment_dir : {}'.format( experiment_dir ) )
    log( DEBUG, 'history_dir : {}'.format( history_dir ) ) 
    log( DEBUG, 'FLOWER_CENTRAL_SERVER : {}:{}'.format( os.getenv( 'FLOWER_CENTRAL_SERVER_IP' ), os.getenv( 'FLOWER_CENTRAL_SERVER_PORT' ) ) )

    # inicial model
    model = LinearRegression() # LogisticRegression()
    set_initial_params( model )
    
    # Create them
    experiment_dir.mkdir( parents = True, exist_ok = True )
    history_dir.mkdir( parents = True, exist_ok = True )

    # Start Flower server for three rounds of federated learning
    if os.getenv( 'NOSECURE' ) is not None:
        log( DEBUG, 'NOSECURE : {}'.format( os.getenv( 'NOSECURE' ) ) )
        history = fl.server.start_server(
            server_address = "{}:{}".format( os.getenv( 'FLOWER_CENTRAL_SERVER_IP' ), os.getenv( 'FLOWER_CENTRAL_SERVER_PORT' ) ), 
            config         = fl.server.ServerConfig( num_rounds = 3 ), 
            strategy       = SaveModelStrategy( 
                on_fit_config_fn      = fit_round,
                min_fit_clients       = 3,
                min_evaluate_clients  = 3,
                min_available_clients = 3
            ),
            certificates   = None
        )
    else:
        history = fl.server.start_server(
            server_address = "{}:{}".format( os.getenv( 'FLOWER_CENTRAL_SERVER_IP' ), os.getenv( 'FLOWER_CENTRAL_SERVER_PORT' ) ), 
            config         = fl.server.ServerConfig( num_rounds = 3 ), 
            strategy       = SaveModelStrategy( 
                on_fit_config_fn      = fit_round,
                min_fit_clients       = 3,
                min_evaluate_clients  = 3,
                min_available_clients = 3
            ),
            certificates   = (
                Path( '.cache/certificates/rootCA_cert.pem' ).read_bytes(),
                Path( '.cache/certificates/server_cert.pem' ).read_bytes(),
                Path( '.cache/certificates/server_key.pem' ).read_bytes(),
            )
        )
    
    # Save the history as a yaml file
    with open( history_dir / 'history.yaml', 'w' ) as fr:
        yaml.dump( history, fr )
