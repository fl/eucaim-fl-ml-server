# EUCAIM FL ML Server


## Introduction

This repository contains the `Dockerfile` and the `server.py` for the server to be used in the [EUCAIM-WP6's demonstration](https://github.com/EUCAIM/fl_demonstrator) for Federated Learning using learning models (aka. logistic regression).

## To be run

This server is to be run with its partner client located [here](https://gitlab.bsc.es/fl/eucaim-fl-ml-client). The orchestration of the experiment is to be done using the [FLmanager](https://gitlab.bsc.es/fl/FLManager).

### Usage

Edit the `config.py` on the `client` folder of the `FLManager` as:

```python
docker_server_image_name = 'registry.gitlab.bsc.es/fl/eucaim-fl-ml-server'
```
